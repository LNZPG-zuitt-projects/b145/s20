function Pokemon(name, level) {
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	// Methods
	this.tackle = function(targetPokemon){
		console.log(this.name + ' has tackled ' + targetPokemon.name);
	};
	this.faint = function(){
		console.log(this.name + ' fainted.');
	};
}

// create instances of our new objects
let pikachu = new Pokemon('Pikachu', 16);
let ratata = new Pokemon('Ratata', 8);